import faunadb, { HasCurrentIdentity } from 'faunadb'

function getStorageToken () {
  // TODO cache the token somewhere (CloudFlare KV?) and request a new one only
  // when the current one has expired.
  return fetch(
    `${OS_AUTH_URL}auth/tokens`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      // Raw template string instead of a JavaScript object because there is no
      // security issue (values come from env vars) and we don't need more.
      body: `{ "auth": { "identity": { "methods": ["password"], "password": { "user": { "name": "${OS_USERNAME}", "domain": { "id": "default" }, "password": "${OS_PASSWORD}" } } }, "scope": { "project": { "name": "${OS_TENANT_NAME}", "domain": { "id": "default" } } } } }`
    }
  ).then((response) => {
    const headers = Object.fromEntries(response.headers)
    // Suprisingly, the needed information is in some header, not in the body.
    return headers['x-subject-token']  
  })
}

// TODO handle other pathname than "/projects/" ones.
// TODO md5 URLs for assets
async function handleRequest(request) {
  try {
    const { pathname } = new URL(request.url)

    if (request.method === "PUT") {
      // TODO improve management of getToken errors
      let storageToken = await getStorageToken()
      if (storageToken === undefined) {
        console.log('Error during getting storage token')
        return new Response(
          `Unexpected error with the storage backend when retrieving ${pathname}. Please inform the adacraft team about this issue.`,
          {status: 500}
        )
      }

      const headers = Object.fromEntries(request.headers)

      const uploadUrl = `${STORAGE_BASE_URL}/${STORAGE_CONTAINER}${pathname}`
      const uploadHeaders = {
        ...headers,
        'X-Auth-Token': storageToken,
      }

      if (/^\/projects\/[0-9a-z]{8}$/.test(pathname)) {    
        // pathname follows the pattern: "/projects/a85ec920" which is an
        // adacraft project.
  
        const faunaToken = 
          // Expected header for the token.
          headers['account-token']
          // "Fauna-Token" name was used once, but is considered obsolete. Remove it
          // sometime.
          || headers['fauna-token']
          || null

        if (faunaToken === null) {
          return new Response(
            'This request requires a "Account-Token" header. Please provide one with a valid token.',
            { status: 401 }
          )  
        }  

        // TODO here, we would like to also authorize URLs like
        // "/projects/debb83fa_data/kjhsksjhg" or any URLs that starts with
        // "/projects/"
        const id = pathname.substring(pathname.lastIndexOf('/') + 1)
    
        const client = new faunadb.Client({
          secret: faunaToken
        })
        const q = faunadb.query
        
        let hasCurrentIdentity
        try {        
          hasCurrentIdentity = await client.query(
              HasCurrentIdentity()
          )
        } catch (error) {
          if (error.name === 'Unauthorized') {
            return new Response(
              'The Fauna token provided in the Account-Token header isn\'t valid. Be sure to provide a valid token.',
              {status: 401}
            )
          }
          throw Error('Unknown internal error during Fauna DB request.')
        }
        if (!hasCurrentIdentity) {
          return new Response(
            'The Fauna token provided in the Account-Token header doesn\'t match an identity. Usually this means the token is the anonymous access token which is only valid for read only opetrations. Be sure to provide a token that is associated to an account.',
            {status: 401}
          )
        }

        const project = await client.query(
          q.Get(q.Match(q.Index('unique_projects_id'), id))
        )

        const identity = await client.query(
          q.CurrentIdentity()
        )

        if (project.data.owner.id === identity.value.id) {
          uploadHeaders['X-Object-Meta-Account-Id'] = identity.value.id
          const sourceTimestamp = headers['source-timestamp'] || null  
          if (sourceTimestamp !== null) {
            uploadHeaders['X-Object-Meta-Source-Timestamp'] = sourceTimestamp
          }
        } else {
          return new Response(
            'The account associated with the Account-Token header hasn\'t writing permission for this project. Please provide a token for another account.',
            {status: 401}
          )
        }
      }
      const response = await fetch(
        uploadUrl,
        {
          method: 'PUT',
          headers: uploadHeaders,
          redirect: 'follow',
          // Just forward the content from this request to the upload URL.
          body: request.body
        }
      )
      const expectedStatus = 201
      if (response.status !== expectedStatus) {
        return new Response(
          'Unexpected error with the storage backend. Please inform the adacraft team about this issue.',
          {status: 500}
        )
      }

      const purgeCacheUrl = `https://api.bunny.net/purge?url=${BUNNY_STORAGE_BASE_URL}${pathname}`
      fetch(
        purgeCacheUrl,
        {
          method: 'POST',
          headers: {
            AccessKey: BUNNY_ACCESS_KEY
          }
        }
      ).catch(err => console.error(err))

      // Forward the response given by the upload to the storage.
      return response
    } else {
      return new Response(
        'bad HTTP method, only accept HTTP PUT',
        {status: 405}
      )
    }
  } catch (error) {
    return new Response(error.message, {status: 500})
  }
}

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})


// TODO better Fauna errors management, for example with something like function
// "getFaunaError" from this article
// https://fauna.com/blog/getting-started-with-fauna-and-cloudflare-workers

// TODO check body size, for now we only rely on the size limit from CloudFlare
// which is 100MB for the free plan.
